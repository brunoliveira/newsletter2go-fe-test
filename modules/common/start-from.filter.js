(function () {
	'use strict';

    /**
     *  Filter receives an array and slices it from the start number.
    */
	angular.module('common').filter('startFrom', () => {
        return (input, start) => {
            start = +start;
            return input.slice(start);
        }
    });
}());