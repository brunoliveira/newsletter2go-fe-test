(function () {
	'use strict';

    /**
     *  Filter receives a date and caclulates the age.
    */
	angular.module('common').filter('age', () => {
        return (dateOfBirth) => {
            const birthday = new Date(dateOfBirth);
            const ageDifMs = Date.now() - birthday.getTime();
            const ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }
    });
}());