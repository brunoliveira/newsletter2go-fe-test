(function () {
    'use strict';

    /**
     *  Allow to download an array of objects as csv file.
     *  Source: https://codepen.io/YuvarajTana/pen/yNoNdZ
    */
    angular.module('common').directive('exportToCsv', function () {
        return {
            restrict: 'A',
            scope: {
                collection: "=",
                csvfilename: "=",
            },
            link: function (scope, element, attrs) {
                const el = element[0];
                element.bind('click', function (e) {
                    let csvString = '';
                    let collection = scope.collection;
                    collection.forEach( e => {
                        delete e.$$hashKey
                        csvString = csvString + Object.values(e).toString() + '\n';
                    });
                    const a = $('<a/>', {
                        style: 'display:none',
                        href: 'data:application/octet-stream;base64,' + btoa(csvString),
                        download: scope.csvfilename
                    }).appendTo('body')
                    a[0].click()
                    a.remove();
                });
            }
        }
    });
}());