(function () {
    'use strict';

    angular.module('user').factory('User', ['$http', '$q', ($http, $q) => {
        const backendUrl = 'http://localhost:8081';
        let users = null;
        let UserService = {};

        /**
         * It retrieves a collection of users from the server or from the memory.
         */
        UserService.getUsers = () => {
            const deferred = $q.defer();
            if (users) {
                deferred.resolve(users);
            } else {
                $http.get(backendUrl + '/users.json')
                    .success(res => {
                        users = res;
                        deferred.resolve(users);
                    })
                    .error(err => {
                        deferred.reject(err);
                    });
            }

            return deferred.promise;
        }

        /**
         * It retrieves a user specified by the id field.
         * @param {(string|number)} userId - User identifier field.
         */
        UserService.getUser = (userId) => {
            const deferred = $q.defer();

            if (users) {
                deferred.resolve(_.find(users, {'id': +userId}));
            }
            else {
                 $http.get(backendUrl + '/users.json')
                    .success(res => {
                        users = res;
                        deferred.resolve(_.find(users, {'id': +userId}));
                    })
                    .error(err => {
                        deferred.reject(err);
                    });
            }
            return deferred.promise;
        }

        /**
         * It removes a user specified by the id field.
         * @param {(string|number)} userId - User identifier field.
         */
        UserService.removeUser = (userId) => {
            const deferred = $q.defer();

            if (users) {
                users = angular.copy(_.reject(users, {'id': +userId}), users);
                deferred.resolve(users);
            }
            else {
                 $http.get(backendUrl + '/users.json')
                    .success(res => {
                        users = _.reject(res, {'id': +userId});
                        deferred.resolve(users);
                    })
                    .error(err => {
                        deferred.reject(err);
                    });
            }
            return deferred.promise;
        }

        /**
         * It updates a specific user.
         * @param {(string|number)} user - User to be updated.
         */
        UserService.updateUser = (user) => {
            const deferred = $q.defer();

            if (users) {
                const userIndex = _.findIndex(users, {'id': user.id});
                users[userIndex] = angular.copy(user);
                deferred.resolve(users[userIndex]);
            }
            else {
                 $http.get(backendUrl + '/users.json')
                    .success(res => {
                        const userIndex = _.findIndex(users, {'id': user.id});
                        users[userIndex] = angular.copy(user);
                        deferred.resolve(users[userIndex]);
                    })
                    .error(err => {
                        deferred.reject(err);
                    });
            }
            return deferred.promise;
        }

        return UserService
    }]);
}());