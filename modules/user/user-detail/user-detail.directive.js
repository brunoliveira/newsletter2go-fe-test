(function () {
    'use strict';

    angular.module('user')
        .controller('ConfirmCancelModalInstanceCtrl', ($scope, $uibModalInstance) => {
            $scope.ok = () => {
                $uibModalInstance.close();
            };

            $scope.cancel = () => {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .directive('userDetail', () => {
            return {
                restrict: 'E',
                controller: ['$scope', '$stateParams', '$uibModal', 'User',
                    ($scope, $stateParams, $uibModal, User) => {
                        $scope.pageTitle = "User Detail";
                        $scope.editMode = false;

                        const _userId = $stateParams.userId;
                        let user = null;

                        User.getUser(_userId).then(user => {
                            $scope.user = user;
                            $scope.editUser = angular.copy($scope.user);
                        });

                        $scope.onEdit = () => {
                            $scope.editMode = true;
                        }

                        $scope.onSave = () => {
                            User.updateUser($scope.editUser).then(user => {
                                $scope.user = angular.copy(user);
                            })
                            $scope.editMode = false;
                        }

                        $scope.onCancel = (userForm) => {
                            if (userForm.$dirty) {
                                const modalInstance = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'confirmCancelEdition.html',
                                    controller: 'ConfirmCancelModalInstanceCtrl',
                                });

                                modalInstance.result.then( () => {
                                    $scope.editMode = false;
                                    $scope.editUser = angular.copy($scope.user);
                                });
                            } else {
                                $scope.editMode = false;
                            }
                        }
                    }
                ],
                templateUrl: 'user/user-detail/user-detail.html'
            }
        });
}());