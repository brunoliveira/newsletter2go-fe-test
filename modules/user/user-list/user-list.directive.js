(function () {
    'use strict';

    angular.module('user')
        .controller('ConfirmDeleteUsersModalInstanceCtrl', ($scope, $uibModalInstance, amountSelectedItems) => {
            $scope.amountSelectedItems = amountSelectedItems;

            $scope.ok = () => {
                $uibModalInstance.close();
            };

            $scope.cancel = () => {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .directive('userList', () => {
            return {
                restrict: 'E',
                controller: ['$scope', '$uibModal','User',
                    ($scope, $uibModal, User) => {
                        $scope.pageTitle = 'Users';
                        $scope.currentPage = 0;
                        $scope.lastPage = null;
                        $scope.pageSize = 10;
                        $scope.selectedUsers = [];

                        const _setlastPage = () => {
                            $scope.lastPage = Math.ceil($scope.users.length / $scope.pageSize) - 1;
                        }

                        User.getUsers().then(users => {
                            $scope.users = users;
                            _setlastPage();
                        });

                        $scope.deleteSelectedUsers = () => {
                            const modalInstance = $uibModal.open({
                                animation: true,
                                templateUrl: 'confirmDeleteUsers.html',
                                controller: 'ConfirmDeleteUsersModalInstanceCtrl',
                                resolve: {
                                    amountSelectedItems: () => {
                                        return $scope.selectedUsers.length;
                                    }
                                }
                            });

                            modalInstance.result.then(() => {
                                $scope.selectedUsers.forEach(user => {
                                    User.removeUser(user.id).then();
                                });

                                $scope.selectedUsers = angular.copy([], $scope.selectedUsers);
                            });
                        }
                    }
                ],
                templateUrl: 'user/user-list/user-list.html'
            }
        });
}());