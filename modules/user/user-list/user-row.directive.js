(function () {
    'use strict';

    angular.module('user')
        .controller('ShowUserModalInstanceCtrl', ($scope, $uibModalInstance, user) => {
            $scope.user = user;

            $scope.ok = () => {
                $uibModalInstance.close();
            };
        })
        .controller('ConfirmDeleteUserModalInstanceCtrl', ($scope, $uibModalInstance, user) => {
            $scope.user = user;

            $scope.ok =  () => {
                $uibModalInstance.close();
            };

            $scope.cancel = () => {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .directive('userRow', () => {
            return {
                restrict: 'A',
                replace: false,
                scope: {
                    user: '=',
                    selectedUsers: '=selectedusers'
                },
                controller: ['$scope', '$uibModal', 'User', ($scope, $uibModal, User) => {
                    $scope.isSelected = false;
                    $scope.show = () => {
                        const modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'showUser.html',
                            controller: 'ShowUserModalInstanceCtrl',
                            resolve: {
                                user:  () => {
                                    return $scope.user;
                                }
                            }
                        });
                    }

                    $scope.confirmDelete = (userId) => {
                        const modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'confirmDeleteUser.html',
                            controller: 'ConfirmDeleteUserModalInstanceCtrl',
                            resolve: {
                                user: () => {
                                    return $scope.user;
                                }
                            }
                        });

                        modalInstance.result.then( () => {
                            User.removeUser($scope.user.id).then();
                        });
                    }

                    $scope.changeSelect = () => {
                        if ($scope.isSelected) {
                            $scope.selectedUsers.push($scope.user);
                        } else {
                            $scope.selectedUsers = angular.copy(
                                _.reject($scope.selectedUsers, {'id': $scope.user.id}),
                                $scope.selectedUsers
                            );
                        }
                    }
                }],
                templateUrl: 'user/user-list/user-row.html'
            }
        });
}());