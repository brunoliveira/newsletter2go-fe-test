(function () {
	'use strict';

	angular.module('app').config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider){
            $stateProvider.state('home', {
                url: '/',
                templateUrl: 'home/home.html'
            }).state('user-list', {
                url: "/users",
                template: "<user-list></user-list>"
            })
            .state('user-detail', {
                url: "/users/:userId",
                template: "<user-detail></user-detail>"
            });

            $urlRouterProvider.otherwise('/');
        }
    ]);
}());