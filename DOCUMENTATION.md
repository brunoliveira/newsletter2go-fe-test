# Newsletter2Go - Front-end Test

A small aplication that lists users and shows users detail.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Prerequisites

You must have [Node and NPM](https://nodejs.org/en/) installed in your machine.

```
Tested versions: Node v7.7.4 and NPM v5.6.0
```

### Installation

Install the development dependecies using NPM.

```
$ npm install
```

Install the application dependecies using Bower

```
$ npm run bower install
```

### Usage

First, run the back-end server.

```
$ npm run start-backend
```

You can find the back-end at: http://localhost:8081

Then, run the front-end application.

```
$ npm run start
```

Go to http://localhost:8080.

## Documentation
List of the application modules.

### Common Module

It has templates, filters and directives that can be used in different components of the application.

#### Filters
* **age**: It receives a date and caculates the age.
* **startFrom**: It receives an array and slices it from the start number.

#### Directives
* **exportToCsv**: It receives a collection and converts it in a csv file to be downloaded.

#### Templates
* **navbar**: It renders a fixed navbar at the top of the page.

### User Module

It has directives and services that lists users and shows user detail.

#### Services
##### User
Allow to retrieve, update and delete users.
* **getUsers**: It retrieves a collection of users.
* **getUser(userId)**: It retrieves a user specified by the *id* field.
* **removeUser(userId)**: It removes a user specified by the *id* field.
* **updateUser(user)**: It updates a specific user.

#### Directives
* **userRow**: It renders a table row with user information and the following actions: *show*, *edit* and *delete* user.
* **userList**: It renders a table of users. It is possible to select a list of users to remove or download them as csv file.
* **userDetail(userID)**: It renders a table with information of the user specified by the *id* field. It is possible to edit the first name and last name of the user.

## Author

* [*Tarcisio Bruno Carneiro Oliveira*](https://github.com/brunoliveira8)

